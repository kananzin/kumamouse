cmake_minimum_required(VERSION 3.10)
project(new_mouse)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_TOOLCHAIN_FILE arm/arm_toolchain.cmake)
include(${CMAKE_TOOLCHAIN_FILE})

set(MBED_TARGET LPC1768)
#set(MBED_TARGET LPC1768)

set(MBEDMOUNT /media/kanato/MBED)
set(SERCON /dev/tty.usbmodem14512)


set(BIN Mbed_binary)
set(SOURCE_FILES src/main.cpp
        src/define.h
        src/Motor.cpp src/Motor.h
        src/Sensor.cpp src/Sensor.h src/Odometry.cpp src/Odometry.h)


include(arm/arm_mbed.cmake)
#add_subdirectory(src/mslm)

message("MBED_OBJECTS: ${MBED_OBJECTS}")
message("TOOLCHAIN: ${TOOLCHAIN}")

add_executable(${BIN} ${SOURCE_FILES} ${MBED_OBJECTS})
target_link_libraries(${BIN} ${MBED_LIBS})