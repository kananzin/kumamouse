# ------------------------------------------------------------------------------
# Copyright by Uwe Arzt mailto:mail@uwe-arzt.de, https://uwe-arzt.de
# under BSD License, see https://uwe-arzt.de/bsd-license/
# ------------------------------------------------------------------------------
INCLUDE(CMakeForceCompiler)
 
#-------------------------------------------------------------------------------
SET(CMAKE_SYSTEM_NAME Generic)
 
#-------------------------------------------------------------------------------
# specify the cross compiler, later on we will set the correct path

set(ARM_GCC_PATH "/home/kanato/CLionProjects/gcc-arm-none-eabi")

CMAKE_FORCE_C_COMPILER(${ARM_GCC_PATH}/bin/arm-none-eabi-gcc GNU)
CMAKE_FORCE_CXX_COMPILER(${ARM_GCC_PATH}/bin/arm-none-eabi-g++ GNU)

#-------------------------------------------------------------------------------
set(TOOLCHAIN TOOLCHAIN_GCC_ARM)

#-------------------------------------------------------------------------------
# define presets
set(USE_RTOS false)
set(USE_NET false)
set(USE_USB false)
set(USE_DSP false)

