# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/kanato/CLionProjects/new_mouse/src/Motor.cpp" "/home/kanato/CLionProjects/new_mouse/cmake-build-debug/CMakeFiles/Mbed_binary.dir/src/Motor.cpp.o"
  "/home/kanato/CLionProjects/new_mouse/src/Odometry.cpp" "/home/kanato/CLionProjects/new_mouse/cmake-build-debug/CMakeFiles/Mbed_binary.dir/src/Odometry.cpp.o"
  "/home/kanato/CLionProjects/new_mouse/src/Sensor.cpp" "/home/kanato/CLionProjects/new_mouse/cmake-build-debug/CMakeFiles/Mbed_binary.dir/src/Sensor.cpp.o"
  "/home/kanato/CLionProjects/new_mouse/src/main.cpp" "/home/kanato/CLionProjects/new_mouse/cmake-build-debug/CMakeFiles/Mbed_binary.dir/src/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../mbed"
  "../mbed/TARGET_LPC1768"
  "../mbed/TARGET_LPC1768/TOOLCHAIN_GCC_ARM"
  "../mbed/TARGET_LPC1768/TARGET_NXP/TARGET_LPC176X"
  "../mbed/TARGET_LPC1768/TARGET_NXP/TARGET_LPC176X/TARGET_MBED_LPC1768"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
