// Created by kanato on 18/11/15.

#ifndef NEW_MOUSE_ODOMETRY_H
#define NEW_MOUSE_ODOMETRY_H


class Odometry {
private:

    int l_old_count;
    int l_delta_count;
    int l_velocity;

    int r_old_count;
    int r_delta_count;
    int r_velocity;

public:
    void _velocity();

};


#endif //NEW_MOUSE_ODOMETRY_H
