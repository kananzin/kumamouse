#include "mbed.h"
#include "Motor.h"
#include "Sensor.h"
#include "Odometry.h"

// 時間で速度を変えろ
// 1秒間に180mm/sで動けるように。
// speedが0で指定された際にattachが不確定になることがある、これを安全に実装できるように。
// P制御
// パルス数から速度mm/sを得られるように

DigitalOut led1(LED1);
DigitalOut led2(LED2);
DigitalOut led3(LED3);
DigitalOut led4(LED4);

BusOut led(LED1, LED2, LED3, LED4);
AnalogOut ref(p18);

Timer time1; //時間の計測

Ticker tick_velocity;

//Motor lm(p23, p24, p25, p26, true);
//Motor rm(p9, p10, p11, p12, false);

//int l_old_count = 0;
//int r_old_count = 0;
//
//void velocity_1(){
//    int l_delta_count = 0;
//    int l_velocity = 0;
//    int r_delta_count = 0;
//    int r_velocity = 0;
//
//    l_delta_count = lm.count() - l_old_count;
//    l_old_count = lm.count();
//    l_velocity = (l_delta_count * 0.2) * 100;
//
//    r_delta_count = rm.count() - r_old_count;
//    r_old_count = rm.count();
//    r_velocity = (r_delta_count * 0.2) * 100;
//
//    printf("%d\r\n", (l_velocity + r_velocity)/2);
//}

int main(){
    Sensor ls(p15), fs(p16), rs(p17), rrs(p19), lls(p20);

    ref = 0.07/3.3;
    led = 0b1111;

//    lm = 100;
//    rm = 100;
//
//    tick_velocity.attach_us(&velocity_1, 10000);

    while(1) {
        printf("LS=%d, FS=%d, RS=%d, RRS=%d, LLS=%d \r\n",
        ls.get_val(),
        fs.get_val(),
        rs.get_val(),
        rrs.get_val(),
        lls.get_val()
        );
        wait(0.3);
    }

//    for(int i = 1; i <= 1000; i++){
//        lm = i;
//        rm = -i;
//        wait(0.1);
//    }

//    time1.start();
//
//    while(1){
//        if(time1.read_ms() > 1000) //1秒たったら抜ける
//            break;
//    }
//
//    lm = 0;
//    rm = 0;
//
//    printf("%d \r\n", lm.count());
//
//    while(1);

//    while(1) {
//        printf("%d \r\n", lm.count()); // sudo screen /dev/serial/by-id/usb-mbed_Microcontroller_101000000000000000000002F7F373CB-if01 9600
//
//        if (lm.count() >= 1000 && rm.count() >= 1000) {
//            lm = 0;
//            rm = 0;
//        }
//    }
    return 0;
}