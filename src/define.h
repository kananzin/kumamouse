// Created by kanato on 18/11/03.

#ifndef NEW_MOUSE_DEFINE_H
#define NEW_MOUSE_DEFINE_H

namespace unit_type{
    using msec = double;
    using sec = double;
    using mm = double;
    using pulse = int;
    using mmpersec = double;
    using pulsepersec = int;
}


#endif //NEW_MOUSE_DEFINE_H
