// Created by kanato on 18/11/03.

#ifndef NEW_MOUSE_MOTOR_H
#define NEW_MOUSE_MOTOR_H

#include "mbed.h"
#include "define.h"

class Motor{
private:
    DigitalOut _m3;
    DigitalOut _clock;
    DigitalOut _cwccw;
    DigitalOut _reset;

    Ticker _time;
    bool _forward;
    int _count;
    unit_type::mmpersec _speed;

public:
    Motor(PinName m3, PinName clock, PinName cwccw, PinName reset, bool forward);

    void step();

    unit_type::pulse count();

    Motor& operator =(unit_type::mmpersec speed);
};


#endif //NEW_MOUSE_MOTOR_H
