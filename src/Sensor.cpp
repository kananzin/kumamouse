// Created by kanato on 18/11/08.

#include "Sensor.h"

Sensor::Sensor(PinName analog_value):_analog_value(analog_value){}

int Sensor::get_val(){
    return  (((_analog_value*3.3) - 2.375)/(-0.0375))*10;
}