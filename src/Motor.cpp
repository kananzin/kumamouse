// Created by kanato on 18/11/03.

#include "Motor.h"

Motor::Motor(PinName m3, PinName clock, PinName cwccw, PinName reset, bool forward)
        :_m3(m3), _clock(clock), _cwccw(cwccw), _reset(reset), _time(), _forward(forward), _count(0){
    _m3 = 0;
    _clock = 0;
    _cwccw = 1;
    _reset = 0;
}

void Motor::step() {
    if(_speed != 0){
        _clock = !_clock;
        if(!_clock) {
            _count++;
        }
    }
}

unit_type::pulse Motor::count() {
    return _count;
}

Motor &Motor::operator=(unit_type::mmpersec speed) {
    _speed = speed;
    if(_speed < 0){
        _cwccw = !_forward;
        _speed = -_speed;
    }else{
        _cwccw = _forward;
    }

    if(_speed != 0){
        _time.attach_us(this, &Motor::step, static_cast<timestamp_t>(1000000.0 / (_speed * 10)));
    }
    return *this;
}



