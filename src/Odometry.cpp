// Created by kanato on 18/11/15.

#include "Motor.h"
#include "Odometry.h"

Motor lm(p23, p24, p25, p26, true);
Motor rm(p9, p10, p11, p12, false);

void Odometry::_velocity() {
    l_delta_count = lm.count() - l_old_count;
    l_old_count = lm.count();
    l_velocity = (l_delta_count * 0.2) * 100;

    r_delta_count = rm.count() - r_old_count;
    r_old_count = rm.count();
    r_velocity = (r_delta_count * 0.2) * 100;

//    printf("%d\r\n", (l_velocity + r_velocity)/2);
}
