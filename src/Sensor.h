// Created by kanato on 18/11/08.

#ifndef NEW_MOUSE_SENSOR_H
#define NEW_MOUSE_SENSOR_H

#include "mbed.h"
#include "define.h"

class Sensor {
private:
    AnalogIn _analog_value;

public:
    Sensor(PinName analog_value);
    int get_val();
};


#endif //NEW_MOUSE_SENSOR_H
